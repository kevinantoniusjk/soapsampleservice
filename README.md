# soapsampleservice



## XML Request Example

```
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header>
        <orac:authenticationheader xmlns:orac="http://www.oracle.com">
            <orac:username>xxx</orac:username>
            <orac:password>xxx</orac:password>
        </orac:authenticationheader>
    </soapenv:Header>
    <soapenv:Body>
        <v1:sampleservicerq xmlns:v1="http://www.oracle.com/external/services/sampleservice/request/v1.0">
            <v1:service_id>1234567890</v1:service_id>
            <v1:order_type>CH</v1:order_type>
            <v1:type>PO</v1:type>
            <v1:trx_id>e6714ec0-b379-11e9-889b-7f7167f4f73d</v1:trx_id>
        </v1:sampleservicerq>
    </soapenv:Body>
</soapenv:Envelope>
```

## Expected XML Response Example

```
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <tns:getResponse xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://www.oracle.com/external/services/sampleservice/request/v1.0">
            <tns:error_code>0000</tns:error_code>
            <tns:error_msg>Success</tns:error_msg>
            <tns:trx_id>e6714ec0-b379-11e9-889b-7f7167f4f73d</tns:trx_id>
        </tns:getResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```
